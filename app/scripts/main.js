$(document).ready(function() {

  $('.carousel').carousel({
    interval: false
  });

  $('.single-items').slick({
    dots: false,
    infinite: true,
    speed: 300,
    slidesToShow: 1,
    arrows: true
  });

  //$('#Carousel_active').active();

  // Animations

  $('.main-slider__features').addClass('animated bounceInLeft');
  $('.main-slider__links').addClass('animated bounceInLeft');
  $('.carousel-caption').addClass('animated bounceInRight');
  $('.main-header').addClass('animated fadeInUp');
  $('.sub-header').addClass('animated fadeInDown');
  $('.main-slider__button').addClass('animated fadeInRightBig');
  $('.section__scroll').addClass('animated fadeInUpBig');
  $('.features__card').addClass('animated zoomInUp');
  $('.features-content').addClass('animated zoomInRight');


  $('#fullpage').fullpage({
    css3: true,
    scrollingSpeed: 1000,
    autoScrolling: true,
    fitToSection: true,
    fitToSectionDelay: 1000,
    scrollBar: false,
    easing: 'easeInOutCubic',
    easingcss3: 'ease',
    loopBottom: false,
    loopTop: false,
    loopHorizontal: true,
    continuousVertical: false,
    continuousHorizontal: false,
    scrollHorizontally: false,
    interlockedSlides: false,
    resetSliders: false,
    fadingEffect: false,
    normalScrollElements: '#element1, .element2',
    scrollOverflow: true,
    scrollOverflowOptions: null,
    touchSensitivity: 15,
    normalScrollElementTouchThreshold: 5,
    bigSectionsDestination: null,
    responsiveWidth: 768,

    //Accessibility
    keyboardScrolling: true,
    animateAnchor: true,
    recordHistory: true,

    //events
    onLeave: function(index, nextIndex, direction) {},
    afterLoad: function(anchorLink, index) {},
    afterRender: function() {},
    afterResize: function() {},
    afterSlideLoad: function(anchorLink, index, slideAnchor, slideIndex) {},
    onSlideLeave: function(anchorLink, index, slideIndex, direction, nextSlideIndex) {},

    scrollOverflowOptions: {
      click: true
    },
    'scrollOverflow': true,
    'verticalCentered': false,
    navigation: true,
    css3: true,
    animateAnchor: true,
    anchors: ['firstPage', 'secondPage', 'thirdPage', 'fourthPage', 'lastPage'],
    onLeave: function(index, nextIndex, direction) {
      if (nextIndex == 2) {
        $('.about-slider').addClass('animated fadeInDown');
        $('.about-slider__companies').addClass('animated rotateInUpLeft');
      }
      if (nextIndex == 3) {
        $('#seo').addClass('animated fadeInUp');
      }
      if (nextIndex == 4) {
        $('.contact-slider__title').addClass('animated bounceInLeft');
        $('.col-xs-4').addClass('animated bounceInDown');
        $('.main-footer').addClass('animated fadeInUp');
        $('.contact-slider__form').addClass('animated fadeInRight');
        $('#anim1').addClass('animated slideInUp');
        $('#anim2').addClass('animated slideInUp');
        $('#anim3').addClass('animated slideInUp');
      }
    }
  });

  $(document).on('click', '#slide-down', function() {
    $.fn.fullpage.moveSectionDown();
  });

  $(document).on('click', '#slide-up', function() {
    $.fn.fullpage.moveSectionUp();
  });

  function initSlider() {
    $('.references').slick({
      dots: false,
      infinite: true,
      speed: 300,
      slidesToShow: 1,
      autoplay: true,
      prevArrow: '<div class="slick-prev"><i class="fa fa-chevron-left"></i></div>',
      nextArrow: '<div class="slick-next"><i class="fa fa-chevron-right"></i></div>'
    });
  }

  $(document).on('ready', function() {
    initSlider();
  });

});